from turtle import *
from functools import reduce
from sys import argv

def lire_ligne_commande():
	sortie = None #La sortie peut ne pas être citée en ligne de commande donc on la déclare en tant que None
	erreur = ""
	if len(argv) == 3 and argv[1] == '-i':
		entree = argv[2]
	elif len(argv) == 5:
		if argv[1] not in ['-i', '-o'] or argv[3] not in ['-i', '-o']:
			erreur = "Paramètres inconnus/inattendus."
		if argv[1] == '-i' and argv[3] != '-i':
			entree = argv[2]
			sortie = argv[4]
		elif argv[1] == '-o' and argv[3] != '-o':
			entree = argv[4]
			sortie = argv[2]
		else:
			erreur = "Paramètres répétés."
	else:
		erreur = "Quantité invalide d'arguments / Premier argument invalide."
	if erreur != "": #On quitte le programme si la lecture ne s'est pas passé comme prévu.
		print("\033[31mERREUR\033[0m: Lecture de la ligne de commande: %s" % erreur)
		quit()
	if sortie == None: #Si l'utilisateur n'a pas donné de nom pour la sortie, on lui donne un nom par défaut
		sortie = "output.py"
	return entree, sortie 


def appliquer_regles(axiome, regles, niveau): #Applique les règles à l'axiome autant de fois que le niveau pour obtenir l'instruction finale à donner au L-System
	for i in range(niveau):
		tmp = reduce(lambda x, y: x+regles[y] if y in regles.keys() else x+y, axiome, "")
		axiome = tmp
	return axiome

def is_number_string(string, name): #Regarde si une chaîne est uniquement composée d'entiers ou de réels dans le cas de l'angle
	point = 0
	for ca in string:
		if ca not in ".0123456789":
			return False
		elif ca == '.':
			point +=1
			if point == 2 or name != "angle": #Ce n'est pas un nombre décimal si il a plus d'1 virgule
				return False
	return True

def obtenir_regles(f):
	l = f.readline()
	erreur = ""
	res = dict()
	while l != "" and l[0] == '\t':
		o = 0 if l[-1] == '\n' else 1 #On crée un offset au cas où il n'y a pas de retour à la ligne à la toute fin du fichier
		if l[1]+l[3]+l[-2+o] == '"="' and l[2] not in res.keys():
			for ca in l[2]+l[4:-2+o]:
				if ca not in "ab+-*[]":
					erreur = "Règle %s invalide." % l[2:-2+o]
			if erreur == "":
				res[l[2]] = l[4:-2+o]
		else:
			erreur = "Format non respecté pour la règle %s" % (l[1:-1] if l[-1] == '\n' else l[1:])
		l = f.readline()
	if res == dict():
		erreur = "Aucune règle définie!"
	return res, l, erreur

def lire_fichier(s_fichier): #Lit et traîte le fichier pour en extraire les instructions à donner à la tortue
	f = open(s_fichier, "r") #Ouverture du fichier
	res = dict()
	regles = dict()
	l = f.readline()
	erreur = ""
	while l != "" and erreur == "": #Tant que le fichier n'est pas fini et qu'il n'ya a pas d'erreur on cherche les informations nécessaires
		if len(l.split(' ')) == 3 and  l.split(' ')[0] == "axiome" and "instruction" not in res.keys() and l.split(' ')[1] == "=" and l.split(' ')[2][0] == l.split(' ')[2][-2] == '"': #On traîte l'axiome séparément
			for ca in l.split(' ')[2][1:-2]:
				if ca not in "ab+-*[]":
					erreur = "Axiome invalide"
			if erreur == "":
				res['instruction'] = l.split(' ')[2][1:-2]
		elif l == "regles =\n" and regles == dict(): #On traîte les règles dans une fonction dédiée
			regles, l, erreur = obtenir_regles(f) #Il faut récupérer la ligne d'après la définition des règles
			continue #Nécessaire sinon on saute une ligne avant l'itération
		elif len(l.split(' ')) == 3 and l.split(' ')[0] in ["angle", "taille", "niveau"] and l.split(' ')[0] not in res.keys() and l.split(' ')[1] == "=" and  is_number_string(l.split(' ')[2][:-1], l.split(' ')[0]): #On traîte l'angle, le niveau et éventuellement la taille
			res[l.split(' ')[0]] = int(l.split(' ')[2]) if l.split(' ')[0] in ["taille", "niveau"] else float(l.split(' ')[2])
		else:
			erreur = "Paramètre %s invalide. Y a-t-il plusieurs instances de paramètres valides?" % l.split(' ')[0]
		l = f.readline()
	f.close() #Fermeture du fichier
	if ('instruction' not in res.keys() or 'niveau' not in res.keys() or 'angle' not in res.keys() or regles == dict()): #On quitte si il manque un paramètre obligatoire
		erreur = "Paramètres obligatoires manquants"
	if (erreur != ""):
		print("\033[31mERREUR\033[0m: Lecture du fichier \033[33m%s\033[0m: %s" % (s_fichier, erreur))
		quit()
	res['instruction'] = appliquer_regles(res['instruction'], regles, res['niveau'])
	res.pop('niveau') #On n'a plus besoin du niveau à ce point là
	return res

# exécute la commande associée au charactère var
def exe_commande(var, dico, sauvegarde):
	sauvetemp = []
	if (var == 'a'):
		pd()
		fd(dico['taille'])
	elif (var == 'b'):
		pu()
		fd(dico['taille'])
	elif (var == '+'):
		rt(dico['angle'])
	elif (var == '-'):
		lt(dico['angle'])
	elif (var == '*'):
		rt(180)
	elif (var == '['):
		# enegistre la coordonnée x et la coordonnée y dans la pile sauvegarde ainsi que l'orientation du stylo pour les réutiliser plus tard
		sauvetemp.append(round(xcor(), 5))
		sauvetemp.append(round(ycor(), 5))
		sauvetemp.append(heading())
		sauvegarde.append(sauvetemp)
	elif (var == ']'):
		# on lève le stylo pour ne pas laisser de marque, pas la peine de le baisser, tous les mouvements baissent ou lèvent le stylo d'eux mêmes
		pu()
		setpos(sauvegarde[-1][0], sauvegarde[-1][1])
		setheading(sauvegarde[-1][2])
	return sauvegarde

# écrit la commande associé au charactère var sur le terminal et sur le fichier python
def ecrire_commande(var, dico, sauvegarde, l):
	if (var == 'a'):
		print("pd(); fd(%s);" % dico['taille'])
		l.write("pd(); fd(%s);\n" % dico['taille'])
	elif (var == 'b'):
		print("pu(); fd(%s);" % dico['taille'])
		l.write("pu(); fd(%s);\n" % dico['taille'])
	elif (var == '+'):
		print("right(%s)" % dico['angle'])
		l.write("right(%s)\n" % dico['angle'])
	elif (var == '-'):
		print("left(%s)" % dico['angle'])
		l.write(("left(%s)\n" % dico['angle']))
	elif (var == '*'):
		print("right(180)")
		l.write("rt 180\n")
	elif (var == ']'):
		l.write("pu()\n")
		print("pu()")
		l.write("setpos(%s, %s)\n" % (sauvegarde[-1][0], sauvegarde[-1][1]))
		print("setpos(%s, %s)" % (sauvegarde[-1][0], sauvegarde[-1][1]))
		l.write("setheading(%s)\n" % sauvegarde[-1][2])
		print("setheading(%s)" % sauvegarde[-1][2])
		# on ne pop la sauvegarde qu'ici car on doit avoir l'élément du dessus pour les deux fonctions
		sauvegarde.pop(-1)
	return sauvegarde

#Fonction principale
entree, sortie = lire_ligne_commande()
sauvegarde = []
dico = lire_fichier(entree)
if 'taille' not in dico.keys(): #Valeur par défaut de la taille si elle n'a pas été mise
	dico['taille'] = 10
l = open(sortie, "w")
l.write("from turtle import *\nspeed(0)\n")
print("from turtle import *\nspeed(0)")
speed(0)
for c in dico['instruction']:
	exe_commande(c, dico, sauvegarde)
	ecrire_commande(c, dico, sauvegarde, l)
l.write("exitonclick()")
print("exitonclick()")
l.close()
exitonclick()
# PROJET L-SYSTEM

### Par Thomas Fortier, Mateo Mongour et Simon Schwaar

Génère un script Python correspondant à un L-System à partir d'un fichier d'entrée. Pour plus de détails concernant les L-Systems, veuillez lire le rapport.

## EXÉCUTION

Le script s'exécute avec la ligne de commande suivante:

`python3 script.py [-i <Fichier d'entrée>] [-o <Fichier de sortie>]`

`-i <Fichier d'entrée>`: Ces arguments sont obligatoires sans quoi le script s'arrêtera. Le `<Ficiher d'entrée>` doit contenir le chemin vers le fichier à faire lire et interpréter au script.

`-o <Fichier de sortie>`: Ces arguments sont optionnels. Le `<Fichier de sortie>` contient le nom du fichier de sortie à générer. Si ces arguments ne sont pas saisis sur la ligne de commande, le nom de sortie `output.py` sera mis par défaut.

## FORMAT DU FICHIER D'ENTRÉE

Le ficiher d'entrée doit suivre un format assez spécifique et strict. Il comporte les champs suivants (qui doivent être uniques et dont l'ordre n'importe pas):

### Axiome

Ce champ est obligatoire et doit être de la forme: `axiome = "<Chaîne>"`. La chaîne de caractères doit exclusivement être faite à partir des caractères `a, b, +, -, *, [, ]`. Ce champ ne doit faire qu'une seule ligne.

### Niveau, Taille, et Angle

Les champs Niveau et Angle sont obligatoires, le champ Taille est optionnel. Ils doivent être de la forme: `[niveau][taille][angle] = <Nombre>`. Le nombre doit être un entier pour le niveau et la taille, mais peut être un réel pour l'angle. Ces champs doivent faire une ligne chacuns. La taille étant optionelle, si elle n'est pas saisie, elle prendra la valeur par défaut de 10.

### Règles

Le champ Règles doit être constitué d'une première ligne qui vaut strictement `regles =`. À partir de là, au moins une ligne doit être indentée par un Tab (\t, et non par des espaces!).

Les lignes indentées doivent être de la forme: `\t"<Caractère>=<Chaîne>"`. Le caractère doit être seul et ne peut avoir qu'une seule ligne qui lui correspond, et la chaîne peut être à longueur variable. Les deux doivent cependant ne contenir que les caractères `a, b, +, -, *, [, ]`.

L'arrêt de l'indentation par des Tab marque la fin du champ Règles.
